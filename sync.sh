#!/bin/bash

PROJECT="wolfapi"
DST_SERVER="10.25.135.141"
SRC_SERVER="10.25.135.58"
DST_PORT="9010"
DATE=`date "+%Y%m%d%H%M%S"`
STR_DATE=`date "+%s"`
backup="backup"

DEPLOY_PATH="/data/web/hf_app/build.$PROJECT/ROOT"
RSYNC_PATH="hefan/build.$PROJECT/ROOT/"
RETURN_CODE="200"
PORT="1922"
date1=`date +%Y%m%d`
port=$1

sync(){
        ssh -t -p $PORT $1 " 
        
          rsync -avP --delete $SRC_SERVER::$RSYNC_PATH $DEPLOY_PATH
        "
}

restart(){
        ssh -t -p $PORT $1 "ps aux |grep wolf-api.jar |grep -v grep |awk '{print \$2}' |xargs kill -9"
        ssh -p $PORT $1 "source /etc/profile ; cd /data/web/hf_app/build.wolfapi/ROOT; nohup java -jar wolf-api.jar --server.port=9010 --spring.profiles.active=dev >/dev/null &"
}

check(){
        STATUS=1
        for i in `seq 1 60`;do
                code=`curl -I -m 10 -o /dev/null -s -w %{http_code} http://$1:9010`
                if [ $code -eq 200 ];then
                        STATUS=0
                        break
                else
                        echo "wolf-api.jar has not started , please wait............"
                        sleep 1
                fi
        done
}

for IP in $DST_SERVER;do
        sync $IP
        restart $IP
#        check $IP
done
