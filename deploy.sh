#!/bin/bash

### init config
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
source /etc/profile

PRE_DIR=`dirname $0`
PROJECT_DIR=/data/web/hf_app/build.wolfapi
PROJECT_NAME=wolf-api

DATE=`date "+%Y%m%d%H%M%S"`
STR_DATE=`date "+%s"`

backup="backup"
#backup
tar -zcvf ROOT."$DATE".tar.gz $PROJECT_DIR/ROOT
mv ROOT.$DATE.tar.gz $backup/


#deploy

echo Deploy:

cd $PROJECT_DIR/$PROJECT_NAME
 git clean -df
 git checkout master
git pull
if [ ! -z $1 ];then
        git checkout $1
        git pull
        git branch -D test
        git checkout -b test
    else
        git checkout master 
        git pull
        git branch -D test
        git checkout -b test
    fi
mvn clean package -DskipTests -Pdev -U -o  

cp $PROJECT_DIR/$PROJECT_NAME/target/wolf-api.jar $PROJECT_DIR/ROOT
echo "package success"
